import Vue from 'vue'

Vue.mixin({
  methods: {
    price (input) {
      try {
        return input.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
      } catch (e) {
        return input
      }
    }
  }
})
