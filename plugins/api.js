export default function ({ $axios }, inject) {
  const api = $axios.create({
    headers: {
      common: {
        'Content-Type': 'application/json',
        Accept: 'application/json'
      }
    }
  })

  inject('api', api)
}
